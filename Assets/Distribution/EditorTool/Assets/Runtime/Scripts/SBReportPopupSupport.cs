/* Created by Thai Ngoc */
/* Spirit Bomb Studios*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SpiritBomb
{   
    public class SBReportPopupSupport 
    {
        public static Action<string> OnShowReport;

        public static void RequestShowReport(string text)
        {
            if(OnShowReport != null)
            {
                OnShowReport.Invoke(text);
            }
        }
    }
}
