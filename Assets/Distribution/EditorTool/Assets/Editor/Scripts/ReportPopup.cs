/* Created by Thai Ngoc */
/* Spirit Bomb Studios*/

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SpiritBomb
{
    public class ReportPopup : EditorWindow
    {
        private static string _report = "";

        private GUIStyle _titleLabelStyle;

        [InitializeOnLoadMethod]
        static void Init()
        {
            SBReportPopupSupport.OnShowReport += Show;
        }

        private void Awake()
        {
            _titleLabelStyle = new GUIStyle(EditorStyles.label)
            {
                fontSize = 14,
                fontStyle = FontStyle.Bold,
                fixedHeight = 20
            };
        }


        public static void Show(string report)
        {
            ReportPopup window = EditorWindow.GetWindow<ReportPopup>(true, title: "SB - Editor - Report", focus: true);
            _report = report;
            window.maxSize = new Vector2(250, 150);
            window.minSize = new Vector2(250, 150);
            window.position = new Rect(700, 500, 250, 150);
            window.Show();

        }

        private void OnGUI()
        {
            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Th�ng b�o!!!", _titleLabelStyle);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.Space(20);

            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            GUILayout.Label(_report);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.FlexibleSpace();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("Okie", GUILayout.Height(30), GUILayout.Width(80)))
            {
                this.Close();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(10);
            GUILayout.EndVertical();
        }
    }
}
